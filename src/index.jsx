import React, { useState } from 'react'
import { render } from 'react-dom'

const onKeyUp = (todos, setTodos) => e => {
  const value = e.target.value
  if (e.key === 'Enter' && value !== '') {
    setTodos([ ...todos, value])
    e.target.value = ''
  }
}

const Todos = () => {
  const [todos, setTodos] = useState([])
  return <div>
    <ul>
      {
        todos.map((todo, index) => <li key={index}>{todo}</li>)
      }
    </ul>
    <input onKeyUp={onKeyUp(todos, setTodos)} />
  </div>

}

const App = () =>
  <Todos />

render(App(), document.getElementById('app'))