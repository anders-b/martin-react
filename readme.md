# react

Behöver ha installerat `node`. Om inte kolla [nvm](https://github.com/nvm-sh/nvm).

JS "librairies" du behöver:

- [parcel-bundler](https://www.npmjs.com/package/parcel-bundler)
- [react](https://www.npmjs.com/package/react)
- [react-dom](https://www.npmjs.com/package/react-dom)

```
mkdir min-react-app
cd min-react-app
npm init -y
npm install parcel-bundler --save-dev
npm install react react-dom --save
```

Kommer ladda ner en jävla massa. Välkommen till "modern" frontend.

Du behöver en `src` mapp där du kör din skit. `parcel` kommer skapa en `dist` och `.cache` inget du vill ha i din git historik. Inte heller `node_modules`, som skapades tidigare. 

```
touch .gitignore
echo "node_modules" >> .gitignore
echo "dist" >> .gitignore
echo ".cache" >> .gitignore
```

Skapa filer i `src`

```
mkdir src
touch src/index.html
touch src/index.jsx
```

Länka `index.jsx` i `index.html`

```html
<script src="index.jsx"></script>
```

Lägg till `dev` och `build` script i `package.json` (skapad när du körde `npm init -y`)

```js
{
  // ...
  "scripts": {
    // ...
    "dev": "parcel src/index.html",
    "build": "parcel build src/index.html"
  },
  // ...
}
```

Bara att köra

```
npm run dev
```

startar en server och ger dig länk till localhost. borde ladda om när du ändra nàgot i dina filer.

